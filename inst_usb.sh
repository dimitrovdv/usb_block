#!/bin/sh
cp -r /opt/flash_serial /etc/udev
cp -r /opt/99-usb.rules /etc/udev/rules.d
cp -r /opt/usb.sh /usr/local/bin
chmod ugo+rwx /usr/local/bin/usb.sh
chown root:root /etc/udev/rules.d/99-usb.rules
chown root:root /etc/udev/flash_serial
udevadm control --reload-rules
