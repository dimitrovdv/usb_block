#!/bin/sh
  serial=$ID_SERIAL_SHORT
  devpath=$DEVPATH

  logoff=
  good=0

  if grep "^$serial\$" /etc/udev/flash_serial
  then
    good=1
  fi

  if [ $good -ne 1 ]
  then
    sessions=$(loginctl list-sessions | sed '1d;$d' | awk '/    ./ { print $1 } ')
    for SESSION in $sessions
    do
      export DISPLAY=$(loginctl show-session $SESSION | grep '^Display' | sed 's/^Display=//')
      USR=$(loginctl show-session $SESSION | grep '^Name' | sed 's/^Name=//')
      USERHOME=$(getent passwd $USR) | cut -d: -f6
      export XAUTHORITY=$USERHOME/.Xauthority
      if [ $(id -u $USR) -gt 999 ]
      then
        if id -Gn $USR | grep -E '(\s|^)(astra-admin|sudo)(\s|$)'
        then
          su $USR -c "$DBUS_SESSION_BUS_ADDRESS notify-send \"Выявлен факт подключения незарегистрированного носителя. Факт нарушения безопасности занесен в системный журнал\""
          printf "%s %s udev: Admin user %s has inserted unauthorized USB disk device with serial %s, not blocking user\n" "$(LANG=C date)" "$(hostname)" "$USR" "$serial" >> /var/log/auth.log
          #lock flash for admin
#          echo -n 1 > /sys/$devpath/device/delete
        else
          passwd -l $USR >/dev/null
          su $USR -c "$DBUS_SESSION_BUS_ADDRESS notify-send \"Выявлен факт подключения незарегистрированного носителя. Учетная запись будет заблокирована через 10 секнуд\""
          printf "%s %s udev: User %s has inserted unauthorized USB disk device with serial %s\n" "$(LANG=C date)" "$(hostname)" "$USR" "$serial" >> /var/log/auth.log
          #lock flash for generic user
          echo -n 1 > /sys/$devpath/device/delete
          logoff=$SESSION
        fi
      fi
    done
    if [ "$logoff" != "" ]
    then
      sleep 10
      loginctl kill-session $logoff
    fi
  fi

  exit 0
